//
//  ViewController.m
//  TheABer
//
//  Created by Bence Budavari on 7/9/15.
//  Copyright (c) 2015 Bence Budavari. All rights reserved.
//

#import "ViewController.h"
#import "AEParametricEqFilter.h"
#import "SVProgressHUD.h"
#import "ResultsViewController.h"

const int max = 10;

@interface ViewController (){
    AEAudioFilePlayer *fileA;
    AEAudioFilePlayer *fileB;
    AEParametricEqFilter *eq;
    
    UIColor *colorTheme;
    
    NSMutableArray *eqChoices;
    NSMutableArray *responses;
    NSMutableArray *answerKey;
    
    NSInteger userAnswer;
    NSInteger random;
    NSInteger correct;
    NSInteger count;

}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Setup Audio
    _audioController = [[AEAudioController alloc] initWithAudioDescription:[AEAudioController nonInterleaved16BitStereoAudioDescription]];
    [_audioController start:nil];
    
    eq = [[AEParametricEqFilter alloc]init];
    
    eqChoices = [[NSMutableArray alloc]init];
    
    for(int i = 250; i <= 8000; i = i *2){
        [eqChoices addObject:[NSNumber numberWithInt:i]];
    }

    eq.bypassed = NO;
    eq.qFactor = .7;
    eq.gain = 9.0;

    [self loadInterface];
}
-(void)viewDidAppear:(BOOL)animated{
    
    count = 0;
    correct = 0;
    responses = [[NSMutableArray alloc]init];
    answerKey = [[NSMutableArray alloc]init];
    
    
    [self retrieveAudioFiles];
    [self nextTest];
}

- (void)loadInterface{
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.5];
    colorTheme = [UIColor colorWithRed:16.0f/255.0f green:182.0f/255.0f blue:196.0f/255.0f alpha:1.0];
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName :colorTheme }];
    self.navigationController.navigationBar.tintColor = colorTheme;
    
    self.playPauseButton.layer.cornerRadius = self.playPauseButton.bounds.size.width/2;
    self.playPauseButton.layer.borderWidth = 2.0;
    self.playPauseButton.layer.borderColor = colorTheme.CGColor;
    self.playPauseButton.clipsToBounds = YES;
    
    self.switcher.tintColor = colorTheme;
    self.switcher.onTintColor = colorTheme;
    
    self.submitButton.titleLabel.textColor = colorTheme;
    self.submitButton.layer.cornerRadius = 12.0;
    self.submitButton.layer.borderColor = colorTheme.CGColor;
    self.submitButton.layer.borderWidth = 2.0;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)retrieveAudioFiles
{
    NSURL *fileForTrackA = [[NSBundle mainBundle] URLForResource:@"PinkNoise" withExtension:@"wav"];
    NSURL *fileForTrackB = [[NSBundle mainBundle] URLForResource:@"PinkNoise" withExtension:@"wav"];
    
    fileA = [AEAudioFilePlayer audioFilePlayerWithURL:fileForTrackA error:NULL];
    
    fileB = [AEAudioFilePlayer audioFilePlayerWithURL:fileForTrackB error:NULL];
    
    fileA.channelIsPlaying = NO;
    fileB.channelIsPlaying = NO;
    
    [self setMasterChannelGroup:[[self audioController] createChannelGroup]];
    
    [self setChannelA:[[self audioController] createChannelGroupWithinChannelGroup:self.masterChannelGroup]];
    [self setChannelB:[[self audioController] createChannelGroupWithinChannelGroup:self.masterChannelGroup]];
    
    [_audioController setVolume:1.0 forChannelGroup:_masterChannelGroup];
    [_audioController setVolume:1.0 forChannelGroup:_channelA];
    [_audioController setVolume:1.0 forChannelGroup:_channelB];
    
    [_audioController addChannels:[NSArray arrayWithObject:fileA] toChannelGroup:_channelA];
    [_audioController addChannels:[NSArray arrayWithObject:fileB] toChannelGroup:_channelB];
    
    fileB.channelIsMuted = YES;
    
    fileA.loop = YES;
    fileB.loop = YES;
    
    [_audioController addFilter:eq toChannel:fileA];

}

#pragma mark - UIPicker DataSource & Delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;

}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [eqChoices count];
    
}


- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    NSLog(@"viewForRow");
    if (!view) {
        view = [[UIView alloc]init];
        view.frame = CGRectMake(0, 0, 100, 40);
    }

    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 40)];
    
    [view addSubview:label];
    
    label.textColor = colorTheme;
    [label setFont:[UIFont fontWithName:@"Helvetica Neue Thin" size: 12]];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = [NSString stringWithFormat:@"%@ Hz",[eqChoices objectAtIndex:row]];
    
    return view;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    userAnswer = row;
}

#pragma mark - Interface Actions
-(IBAction)submitButtonPressed:(id)sender{
        count++;

        if ([eqChoices objectAtIndex:userAnswer] == [eqChoices objectAtIndex:random]) {
            NSLog(@"You are correct!");
            correct ++;
            [SVProgressHUD showSuccessWithStatus:@"Correct"];
        }else{
            NSLog(@"You are incorrect!");
            [SVProgressHUD showErrorWithStatus:@"Incorrect"];
        }
        
        [responses addObject: [eqChoices objectAtIndex:userAnswer]];
    
        if (count == max) {
            [self performSegueWithIdentifier:@"showResults" sender:self];
        }else{
            [self nextTest];
        }
 
}

- (IBAction)switchValueChanged:(id)sender {
    eq.bypassed = !self.switcher.on;
}


- (IBAction)playPauseButtonPressed:(id)sender {
    
    fileA.channelIsPlaying = !fileA.channelIsPlaying;
    fileB.channelIsPlaying = !fileB.channelIsPlaying;
    
    UIImage *buttonImage;
    
    if (fileA.channelIsPlaying) {
        buttonImage = [UIImage imageNamed:@"music-player7"];
    }else{
        buttonImage = [UIImage imageNamed:@"play124"];
    }
    
    [self.playPauseButton setImage:buttonImage forState:UIControlStateNormal];
}


-(void)nextTest{
    random = arc4random_uniform((int)[eqChoices count]);
    eq.centerFrequency = [[eqChoices objectAtIndex:random] doubleValue];
    [answerKey addObject: [NSNumber numberWithDouble:eq.centerFrequency]];
    NSLog(@"Next Button Pressed!");
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    ResultsViewController *vc = (ResultsViewController*)segue.destinationViewController;
    
    vc.answerKey = [[NSMutableArray alloc]initWithArray:answerKey];
    vc.responses = [[NSMutableArray alloc]initWithArray:responses];
    vc.correct = correct;
}




@end
