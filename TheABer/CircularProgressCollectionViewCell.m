//
//  CircularProgressCollectionViewCell.m
//  TheABer
//
//  Created by Bence Budavari on 11/23/15.
//  Copyright (c) 2015 Bence Budavari. All rights reserved.
//

#import "CircularProgressCollectionViewCell.h"

@implementation CircularProgressCollectionViewCell

- (IBAction)showDetailedViewButtonPressed:(id)sender {
    [_delegate loadDetailedView];
}
@end
