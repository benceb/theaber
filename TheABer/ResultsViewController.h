//
//  ResultsViewController.h
//  TheABer
//
//  Created by Bence Budavari on 11/23/15.
//  Copyright (c) 2015 Bence Budavari. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CircularProgressCollectionViewCell.h"

@interface ResultsViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout, CircularProgressCellDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *resultsCollectionView;

@property NSMutableArray *answerKey;
@property NSMutableArray *responses;
@property NSInteger *correct;


@end
