//
//  CircularProgressCollectionViewCell.h
//  TheABer
//
//  Created by Bence Budavari on 11/23/15.
//  Copyright (c) 2015 Bence Budavari. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KAProgressLabel.h"

@protocol CircularProgressCellDelegate<NSObject>

-(void)loadDetailedView;

@end

@interface CircularProgressCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) id delegate;

@property (weak, nonatomic) IBOutlet KAProgressLabel *circularProgress;

- (IBAction)showDetailedViewButtonPressed:(id)sender;

@end
