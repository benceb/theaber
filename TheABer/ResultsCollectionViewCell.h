//
//  ResultsCollectionViewCell.h
//  TheABer
//
//  Created by Bence Budavari on 11/23/15.
//  Copyright © 2015 Bence Budavari. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultsCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *questionNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *answerLabel;
@property (weak, nonatomic) IBOutlet UILabel *responseLabel;
@property (weak, nonatomic) IBOutlet UILabel *resultEmojiLabel;


@end
