 //
//  ViewController.h
//  TheABer
//
//  Created by Bence Budavari on 7/9/15.
//  Copyright (c) 2015 Bence Budavari. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TheAmazingAudioEngine.h"

@interface ViewController : UIViewController<UIPickerViewDataSource, UIPickerViewDelegate>

//Interface
@property (strong, nonatomic) IBOutlet UISwitch *switcher;
@property (weak, nonatomic) IBOutlet UIButton *playPauseButton;
@property (weak, nonatomic) IBOutlet UIPickerView *answerPicker;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;

//Audio
@property(nonatomic, strong) AEAudioController *audioController;
@property (nonatomic)           AEChannelGroupRef           masterChannelGroup;
@property (nonatomic)           AEChannelGroupRef           channelA;
@property(nonatomic)            AEChannelGroupRef           channelB;

- (IBAction)playPauseButtonPressed:(id)sender;
- (IBAction)submitButtonPressed:(id)sender;



@end

