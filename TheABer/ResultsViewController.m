//
//  ResultsViewController.m
//  TheABer
//
//  Created by Bence Budavari on 11/23/15.
//  Copyright (c) 2015 Bence Budavari. All rights reserved.
//

#import "ResultsViewController.h"
#import "ResultsCollectionViewCell.h"


@interface ResultsViewController (){
    BOOL collectionViewOpen;
}

@end

@implementation ResultsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    collectionViewOpen = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

#pragma mark - UICollectionView Datasource & Delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section{
    NSInteger count;
    if (!collectionViewOpen) {
        count = 1;
    }else{
        count =  self.responses.count + 1;
    }
    return count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ResultsCollectionViewCell *cell;
    if (!collectionViewOpen) {

        CircularProgressCollectionViewCell *cell =[collectionView dequeueReusableCellWithReuseIdentifier:@"circle" forIndexPath:indexPath];
        
        cell.delegate = self;
        UIColor *colorTheme = [UIColor colorWithRed:16.0f/255.0f green:182.0f/255.0f blue:196.0f/255.0f alpha:1.0];
        [cell.circularProgress setProgressColor:colorTheme];
        [cell.circularProgress setTrackColor:[UIColor whiteColor]];
        [cell.circularProgress setFillColor:[UIColor clearColor]];
        
        CGFloat correctFloat = [[NSNumber numberWithInt:self.correct]floatValue];
        CGFloat totalFloat = [[NSNumber numberWithInt:self.answerKey.count]floatValue];
        [cell.circularProgress setText:[NSString stringWithFormat:@"%f%", correctFloat/totalFloat * 100]];;
        [cell.circularProgress setTextColor:colorTheme];

        [cell.circularProgress setProgress:(correctFloat/totalFloat)];
        
        return cell;

    }else{
        if (indexPath.row == 0) {
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"results" forIndexPath:indexPath];
            [cell.questionNumberLabel setText:@"#"];
        }else{
    
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"results" forIndexPath:indexPath];
    
    NSString *answer =[ NSString stringWithFormat:@"%@", [self.answerKey objectAtIndex:indexPath.row - 1]];
    NSString *response =[NSString stringWithFormat:@"%@", [self.responses objectAtIndex:indexPath.row - 1]];
    
    [cell.questionNumberLabel setText:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
    [cell.answerLabel setText:answer];
    [cell.responseLabel setText:response];
    
            if ([response isEqualToString:answer]) {
                [cell.resultEmojiLabel setText:@"😃"];
            }else{
                [cell.resultEmojiLabel setText:@"❌"];
            }
        }
    }

    return cell;
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {

    if (!collectionViewOpen) {
        return CGSizeMake(330, 296);
    }else{
        return CGSizeMake(330, 74);
    }

}


#pragma mark - CircularProgressCellDelegate
-(void)loadDetailedView{
    collectionViewOpen = YES;
    [self.resultsCollectionView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, self.resultsCollectionView.numberOfSections)]];
}

@end
